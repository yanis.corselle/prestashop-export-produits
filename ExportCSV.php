<?php
/**
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class ExportCSV extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'ExportCSV';
        $this->tab = 'others';
        $this->version = '1.0.0';
        $this->author = 'Yanis Corselle at Ecig & Zen';
        $this->need_instance = 0;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Export CSV des Produits');
        $this->description = $this->l('Ce module permet d\'exporter les informations des produits au format CSV. Il peut être appelé par une tâche CRON.');

        $this->confirmUninstall = $this->l('');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        Configuration::updateValue('EXPORTCSV_LIVE_MODE', false);

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader');
    }

    public function uninstall()
    {
        Configuration::deleteByName('EXPORTCSV_LIVE_MODE');

        return parent::uninstall();
    }

    public  static function CreationCSV($idpro, $fichier)
    {
        $titre = Db::getInstance()->getValue('
	SELECT `name`
	FROM `'._DB_PREFIX_.'product_lang`
	WHERE `id_product` = '.$idpro);
        $recapitulatif = Db::getInstance()->getValue('
	SELECT `description_short`
	FROM `'._DB_PREFIX_.'product_lang`
	WHERE `id_product` = '.$idpro);

        $description = Db::getInstance()->getValue('
	SELECT `description`
	FROM `'._DB_PREFIX_.'product_lang`
	WHERE `id_product` = '.$idpro);

        $id_image = Db::getInstance()->getValue('
	SELECT `id_image`
	FROM `'._DB_PREFIX_.'image`
	WHERE `id_product` = '.$idpro);

        $id_marque = Db::getInstance()->getValue('
	SELECT `id_manufacturer`
	FROM `'._DB_PREFIX_.'product`
	WHERE `id_product` = '.$idpro);

        $marque = Db::getInstance()->getValue('
	SELECT `name`
	FROM `'._DB_PREFIX_.'manufacturer`
	WHERE `id_manufacturer` = '.$id_marque);

        $id_category_default = Db::getInstance()->getValue('
	SELECT `id_category_default`
	FROM `'._DB_PREFIX_.'product`
	WHERE `id_product` = '.$idpro);

        $id_category_parent = Db::getInstance()->getValue('
	SELECT `id_parent`
	FROM `'._DB_PREFIX_.'category`
	WHERE `id_category` = '.$id_category_default);

        $category_parent = Db::getInstance()->getValue('
	SELECT `name`
	FROM `'._DB_PREFIX_.'category_lang`
	WHERE `id_category` = '.$id_category_parent);

        if($id_category_parent != 0)
        {
            $id_category_parent1 = Db::getInstance()->getValue('
	SELECT `id_parent`
	FROM `'._DB_PREFIX_.'category`
	WHERE `id_category` = '.$id_category_parent);

            $category_parent1 = Db::getInstance()->getValue('
	SELECT `name`
	FROM `'._DB_PREFIX_.'category_lang`
	WHERE `id_category` = '.$id_category_parent1);

            if($id_category_parent1 != 0)
            {
                $id_category_parent2 = Db::getInstance()->getValue('
	SELECT `id_parent`
	FROM `'._DB_PREFIX_.'category`
	WHERE `id_category` = '.$id_category_parent1);

                $category_parent2 = Db::getInstance()->getValue('
	SELECT `name`
	FROM `'._DB_PREFIX_.'category_lang`
	WHERE `id_category` = '.$id_category_parent2);
            }
        }

        $category_default = Db::getInstance()->getValue('
	SELECT `name`
	FROM `'._DB_PREFIX_.'category_lang`
	WHERE `id_category` = '.$id_category_default);

        $reference = Db::getInstance()->getValue('
	SELECT `reference`
	FROM `'._DB_PREFIX_.'product`
	WHERE `id_product` = '.$idpro);

        $id_default_combination = Db::getInstance()->getValue('
	SELECT `cache_default_attribute`
	FROM `'._DB_PREFIX_.'product`
	WHERE `id_product` = '.$idpro);


        if($id_default_combination != 0)
        {
            $reference = Db::getInstance()->getValue('
            SELECT `reference`
            FROM `'._DB_PREFIX_.'product_attribute`
            WHERE `id_product_attribute` = ' . $id_default_combination);
            $ligne = mb_convert_encoding(array("information_produit", $reference, $titre, $recapitulatif, $description, $marque, $category_default, 1, $category_parent, $category_parent1, $category_parent2), "UTF-8"); // 1 = produit avec declinaison(s), 1 = produit de base (pas d'informations sur les declinaisons
            fputcsv($fichier, $ligne);
            $tableauIDs = Db::getInstance()->ExecuteS('
        SELECT `id_product_attribute`
        FROM `'._DB_PREFIX_.'product_attribute`
        WHERE `id_product` = '.$idpro);
            foreach ( $tableauIDs as $id_decli ) {
                $id_decli = $id_decli['id_product_attribute'];
                $decli_defaut = 0;
                if($id_decli == $id_default_combination)
                {
                    $decli_defaut = 1;
                }
                $ids_attribute = Db::getInstance()->ExecuteS('
            SELECT `id_attribute`
            FROM `'._DB_PREFIX_.'product_attribute_combination`
            WHERE `id_product_attribute` = ' . $id_decli);
                $incrementation_id_attribute = 0;
                foreach ($ids_attribute as $id_attribute) {
                    $id_attribute_group[$incrementation_id_attribute] = Db::getInstance()->getValue('
                SELECT `id_attribute_group`
                FROM `' . _DB_PREFIX_ . 'attribute`
                WHERE `id_attribute` = ' . $id_attribute['id_attribute']);
                    $attribute[$incrementation_id_attribute] = Db::getInstance()->getValue('
                SELECT `name`
                FROM `' . _DB_PREFIX_ . 'attribute_lang`
                WHERE `id_attribute` = ' . $id_attribute['id_attribute']);
                    $attribute_group[$incrementation_id_attribute] = Db::getInstance()->getValue('
                SELECT `name`
                FROM `' . _DB_PREFIX_ . 'attribute_group_lang`
                WHERE `id_attribute_group` = ' . $id_attribute_group[$incrementation_id_attribute]);
                    $incrementation_id_attribute++;
                }
                $reference_decli = Db::getInstance()->getValue('
            SELECT `reference`
            FROM `'._DB_PREFIX_.'product_attribute`
            WHERE `id_product_attribute` = ' . $id_decli);

                $ligne = mb_convert_encoding(array("information_declinaison", $reference_decli, $attribute_group[0], $attribute[0], $attribute_group[1], $attribute[1], $attribute_group[2], $attribute[2], $attribute_group[3], $attribute[3], $decli_defaut), "UTF-8");
                fputcsv($fichier, $ligne);
            }
        }

        else
        {
            $ligne = mb_convert_encoding(array("information_produit", $reference, $titre, $recapitulatif , $description, $marque, $category_default, 1, $category_parent, $category_parent1, $category_parent2), "UTF-8"); //0 = pas de declinaison
            fputcsv($fichier, $ligne);
        }

    }


    public  static function RecuperationIDs()
    {
        $tableauIDs = Db::getInstance()->ExecuteS('
	SELECT *
	FROM `'._DB_PREFIX_.'product`');
        return $tableauIDs;
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitExportCSVModule')) == true) {
            $this->postProcess();
        }

        $this->context->smarty->assign('module_dir', $this->_path);

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        unlink(_PS_MODULE_DIR_.'/ExportCSV/export.csv');
        $csv = fopen(_PS_MODULE_DIR_.'/ExportCSV/export.csv', 'w');
        $tableauIDs = $this->RecuperationIDs();
        $ligne = array("type_information", "reference", "titre ou groupe de declinaison", "type de declinaison / recapitulatif", "description / incrementation_decli", "marque", "categorie", "possede des declinaisons (0=non / 1=oui)");
        fputcsv($csv, $ligne);
        foreach ( $tableauIDs as $f ) {
            $this->CreationCSV($f['id_product'], $csv);
            }
        fclose($csv);
        return $output;
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitExportCSVModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Settings'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Live mode'),
                        'name' => 'EXPORTCSV_LIVE_MODE',
                        'is_bool' => true,
                        'desc' => $this->l('Use this module in live mode'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-envelope"></i>',
                        'desc' => $this->l('Enter a valid email address'),
                        'name' => 'EXPORTCSV_ACCOUNT_EMAIL',
                        'label' => $this->l('Email'),
                    ),
                    array(
                        'type' => 'password',
                        'name' => 'EXPORTCSV_ACCOUNT_PASSWORD',
                        'label' => $this->l('Password'),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(
            'EXPORTCSV_LIVE_MODE' => Configuration::get('EXPORTCSV_LIVE_MODE', true),
            'EXPORTCSV_ACCOUNT_EMAIL' => Configuration::get('EXPORTCSV_ACCOUNT_EMAIL', 'contact@prestashop.com'),
            'EXPORTCSV_ACCOUNT_PASSWORD' => Configuration::get('EXPORTCSV_ACCOUNT_PASSWORD', null),
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
    }
}
